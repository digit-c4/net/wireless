§version: "1"
namespace eu.europa.ec.snet.wireless.configure_wireless_controller_ap_awx

@documentation("A representation of a complete object to configure an access-point on a Cisco WLAN Controller")
structure configure_wireless_controller_ap_awx {
    @required
    @documentation("Dict object from the device (access point) that triggered the webhook in Netbox")
    awx_webhook_payload: String,

    @required
    @documentation("Device ID for the device that triggered the webhook in Netbox")
    device_id: Integer,

    @required
    @documentation("URL path (without base URL) to the device in Netbox that triggered the webhook")
    url_device_id: String,

    @required
    @documentation("URL of the Netbox instance")
    netbox_base_url: String,

    @required
    @documentation("URL of the applicable Netbox instance")
    NETBOX_URL: String,

    @required
    @documentation("Token to be used when connecting with Netbox")
    NETBOX_TOKEN: String,

    @required
    @documentation("Vault role id")
    H_WLC_role_id: String,

    @required
    @documentation("Vault secret id")
    H_WLC_secret_id: String,

    @required
    @documentation("Vault url")
    H_url: String,

    @required
    @documentation("Abbreviation of the applicable site")
    @length(min: 3, max: 3)
    H_site_abbreviation: String
}