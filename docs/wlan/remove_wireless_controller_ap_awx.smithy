§version: "1"
namespace eu.europa.ec.snet.wireless.remove_wireless_controller_ap_awx

@documentation("A representation of a complete object to configure an access-point on a Cisco WLAN Controller")
structure remove_wireless_controller_ap_awx {
    @required
    @documentation("Dict object from the device (access point) that triggered the webhook in Netbox")
    awx_webhook_payload: String,

    @required
    @documentation("Request ID of the removal request in Netbox")
    request_id: Integer,

    @required
    @documentation("URL of the Netbox instance")
    netbox_base_url: String,

    @required
    @documentation("URL of the applicable Netbox instance")
    NETBOX_URL: String,

    @required
    @documentation("Token to be used when connecting with Netbox")
    NETBOX_TOKEN: String,

    @required
    @documentation("Vault role id")
    H_WLC_role_id: String,

    @required
    @documentation("Vault secret id")
    H_WLC_secret_id: String,

    @required
    @documentation("Vault url")
    H_url: String,

    @required
    @documentation("Abbreviation of the applicable site")
    @length(min: 3, max: 3)
    H_site_abbreviation: String
}