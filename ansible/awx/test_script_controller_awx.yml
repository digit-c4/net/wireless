---
- hosts: localhost
  connection: network_cli
  gather_facts: false
  become: true
  vars:
    netbox_base_url: "{{ NETBOX_URL | default('https://vnetbox-dev.dev.snmc.cec.eu.int') }}"
  
  tasks:
    # Install all pip requirements
    - name: Install hvac pip package  #prerequisite for hashicorp collection
      ansible.builtin.shell:
        cmd: pip install hvac
    - name: Install pynetbox pip package  #prerequisite for netbox nb_lookup queries
      ansible.builtin.shell:
        cmd: pip install pynetbox
    - name: Install dnspython pip package  #prerequisite for DNS lookup
      ansible.builtin.shell:
        cmd: pip install dnspython

    # Get the details of the active and standby WLAN Controller from Netbox
    - name: Request active and standby WLAN Controller device details from Netbox for the site
      set_fact:
        device_data_active: "{{ query('netbox.netbox.nb_lookup','virtual-machines', api_filter='role=wlc tag=wifi_cluster_active site='~site_name, api_endpoint=netbox_base_url, validate_certs=false, token=NETBOX_TOKEN) }}"
        device_data_standby: "{{ query('netbox.netbox.nb_lookup','virtual-machines', api_filter='role=wlc tag=wifi_cluster_standby site='~site_name, api_endpoint=netbox_base_url, validate_certs=false, token=NETBOX_TOKEN) }}"

    # Fail if the number of controllers retrieved is not equal to one for each type.
    - name: Quit if the number of WLAN Controllers retrieved is not equal to one for active and/or standby.
      fail: msg="Less or more then one WLAN Controllers retrieved for active and/or standby!"
      when: (device_data_active|length != 1) or (device_data_standby|length != 1)

    # Extract device id's
    - name: Extract device id's for the device
      set_fact:
        device_id_active: "{{ device_data_active[0].value.id }}"
        device_id_standby: "{{ device_data_standby[0].value.id }}"

    - name: Debugging
      debug:
        msg:
          - "Device Data retrieved for active: {{ device_data_active }}"
          - "Device Data retrieved for standby: {{ device_data_standby }}"

    # Get device interface details from Netbox (implemented for virtual appliances, physical appliances require different url)
    - name: Request interface details from Netbox for the devices
      set_fact:
        device_interfaces_active: "{{ query('netbox.netbox.nb_lookup','virtualization-interfaces', api_endpoint=netbox_base_url, api_filter='virtual_machine_id='~device_id_active, validate_certs=false, token=NETBOX_TOKEN) }}"
        device_interfaces_standby: "{{ query('netbox.netbox.nb_lookup','virtualization-interfaces', api_endpoint=netbox_base_url, api_filter='virtual_machine_id='~device_id_standby, validate_certs=false, token=NETBOX_TOKEN) }}"

    # Get device IP address details from Netbox
    - name: Request IP address details from Netbox for the devices
      set_fact:
        device_ip_addressing_active: "{{ query('netbox.netbox.nb_lookup','ip-addresses', api_endpoint=netbox_base_url, api_filter='virtual_machine_id='~device_id_active, validate_certs=false, token=NETBOX_TOKEN) }}"
        device_ip_addressing_standby: "{{ query('netbox.netbox.nb_lookup','ip-addresses', api_endpoint=netbox_base_url, api_filter='virtual_machine_id='~device_id_standby, validate_certs=false, token=NETBOX_TOKEN) }}"

    - name: Debugging
      debug:
        msg:
          - "IP data retrieved for active {{ device_ip_addressing_active }}"
          - "IP data retrieved for standby {{ device_ip_addressing_standby }}"

# Set local and remote cluster link IP on active WLAN controller
- hosts: "{{ hostvars.localhost.device_data_active[0].value.name }}"
  connection: network_cli
  gather_facts: false
  become: true
  vars_files:
    - ../vars/WLC_connection_vars_ldap_awx.yml

  tasks:
    - name: Test script
      include_tasks: ../tasks/set_wireless_license.yml
      vars:
        license_level: advantage #essentials
      
