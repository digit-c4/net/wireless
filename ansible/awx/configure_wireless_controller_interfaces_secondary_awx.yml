# This playbook configures the interfaces on the primary controller as part of the cluster creation process
---
- hosts: localhost
  connection: network_cli
  gather_facts: false
  become: true
  vars:
    netbox_base_url: "{{ NETBOX_URL | default('https://vnetbox-dev.dev.snmc.cec.eu.int') }}"
  
  tasks:
    # Install all pip requirements
    - name: Install hvac pip package  #prerequisite for hashicorp collection
      ansible.builtin.shell:
        cmd: pip install hvac
    - name: Install pynetbox pip package  #prerequisite for netbox nb_lookup queries
      ansible.builtin.shell:
        cmd: pip install pynetbox
    - name: Install dnspython pip package  #prerequisite for DNS lookup
      ansible.builtin.shell:
        cmd: pip install dnspython
    - name: Install netaddr pip package # prerequisite for network address filtering
      ansible.builtin.shell:
        cmd: pip install netaddr

    # Get the details of the standby WLAN Controller from Netbox
    - name: Request standby WLAN Controller device details from Netbox for the site
      set_fact:
        device_data_standby: "{{ query('netbox.netbox.nb_lookup','virtual-machines', api_filter='role=wlc tag=wifi_cluster_standby site='~site_name, api_endpoint=netbox_base_url, validate_certs=false, token=NETBOX_TOKEN) }}"

    # Fail if the number of controllers retrieved is not equal to one.
    - name: Quit if the number of WLAN Controllers retrieved is not equal to one.
      fail: msg="Less or more then one WLAN Controllers retrieved!"
      when: device_data_standby|length != 1

    # Extract device id
    - name: Extract the device id
      set_fact:
        device_id: "{{ device_data_standby[0].value.id }}"

    # Get device interface details from Netbox (implemented for virtual appliances, physical appliances require different url)
    - name: Request interface details from Netbox for the device
      set_fact:
        device_interfaces: "{{ query('netbox.netbox.nb_lookup','virtualization-interfaces', api_endpoint=netbox_base_url, api_filter='virtual_machine_id='~device_id, validate_certs=false, token=NETBOX_TOKEN) }}"

    # Get device IP address details from Netbox
    - name: Request IP address details from Netbox for the device
      set_fact:
        device_ip_addressing: "{{ query('netbox.netbox.nb_lookup','ip-addresses', api_endpoint=netbox_base_url, api_filter='virtual_machine_id='~device_id, validate_certs=false, token=NETBOX_TOKEN) }}"
    
- hosts: "{{ hostvars.localhost.device_data_standby[0].value.name }}"
  connection: network_cli
  gather_facts: false
  become: true
  vars_files:
    - ../vars/WLC_connection_vars_awx.yml

  tasks:
    - name: Set Ansible timeout
      set_fact:
        ansible_command_timeout: 120
    
    # Short pause to allow the controller to be fully booted
    - name: Pause for 30s
      pause:
        seconds: 30
        
    - name: Wait for the standby WLAN Controller to be available after initial deployment.
      include_tasks: ../tasks/wait_node_active.yml

    # Configure the interfaces on the standby virtual WLAN controller
    - name: Configure all interfaces
      include_tasks: ../tasks/create_interface_configuration.yml
      vars:
        - interface_name: "{{ inner_item.value.name }}"
        - interface_description: "{{ inner_item.value.description }}"
        - interface_ip_addr_and_mask: "{{ hostvars.localhost.device_ip_addressing | selectattr('value.assigned_object.name', 'eq', inner_item.value.name) | map(attribute='value.address') }}"
        - interface_ip_role: "{{ hostvars.localhost.device_ip_addressing | selectattr('value.assigned_object.name', 'eq', inner_item.value.name) | map(attribute='value.role') }}"
        - interface_status: "{{ inner_item.value.enabled }}"
        - interface_untagged_vlan_id: "{{ inner_item.value.untagged_vlan }}"
        - interface_mode: "{{ inner_item.value.mode }}"
        - vrf_name: "{{ inner_item.value.vrf.name | default('') }}"
      with_items: "{{ hostvars.localhost.device_interfaces }}"
      loop_control:
        loop_var: inner_item
    
    # Configure all the required routes
    - name: Configure static routes
      include_tasks: ../tasks/create_ip_route.yml
      vars:
        - route_destination: "{{ inner_item.destination }}"
        - route_gateway: "{{ inner_item.gateway }}"
        - route_vrf: "{{ inner_item.vrf }}"
      with_items: "{{ hostvars.localhost.device_data_standby[0].value.config_context.static_routing }}"
      loop_control:
        loop_var: inner_item

    - name: Save running to startup when modified
      ios_config:
        save_when: modified